
  document.addEventListener('DOMContentLoaded', function() {
    const themeButton = document.getElementById('themeButton');
    const currentTheme = localStorage.getItem('theme');

    if (currentTheme) {
      document.body.classList.add(currentTheme);
    }

    themeButton.addEventListener('click', function() {
      if (document.body.classList.contains('dark-theme')) {
        document.body.classList.remove('dark-theme');
        localStorage.removeItem('theme');
      } else {
        document.body.classList.add('dark-theme');
        localStorage.setItem('theme', 'dark-theme');
      }
    });
  });
